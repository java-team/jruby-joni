#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

SOURCE=jruby-joni
DIR=${SOURCE}-$2

git clone http://github.com/jruby/joni.git $DIR
(cd $DIR && git checkout $2)

tar cfz ../${SOURCE}_$2.orig.tar.gz --exclude .git $DIR

rm -rf $DIR ../$2
echo 'created orig tarball successfully'
